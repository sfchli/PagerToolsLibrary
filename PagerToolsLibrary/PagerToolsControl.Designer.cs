﻿namespace PagerToolsLibrary
{
    partial class PagerToolsControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip_main = new System.Windows.Forms.ToolStrip();
            this.tbtn_first = new System.Windows.Forms.ToolStripButton();
            this.tbtn_up = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ttxt_current = new System.Windows.Forms.ToolStripTextBox();
            this.tlab_total = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtn_next = new System.Windows.Forms.ToolStripButton();
            this.tbtn_last = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tcomb_current = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tcom_num = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbtn_refresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tlab_datacount = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip_main.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip_main
            // 
            this.toolStrip_main.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbtn_first,
            this.tbtn_up,
            this.toolStripSeparator1,
            this.ttxt_current,
            this.tlab_total,
            this.toolStripSeparator2,
            this.tbtn_next,
            this.tbtn_last,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.tcomb_current,
            this.toolStripLabel3,
            this.toolStripSeparator4,
            this.toolStripLabel1,
            this.tcom_num,
            this.toolStripLabel4,
            this.toolStripSeparator5,
            this.tbtn_refresh,
            this.toolStripSeparator6,
            this.tlab_datacount});
            this.toolStrip_main.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_main.Name = "toolStrip_main";
            this.toolStrip_main.Size = new System.Drawing.Size(712, 25);
            this.toolStrip_main.TabIndex = 1;
            this.toolStrip_main.Text = "toolStrip1";
            // 
            // tbtn_first
            // 
            this.tbtn_first.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtn_first.Image = global::PagerToolsLibrary.Properties.Resources.first;
            this.tbtn_first.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtn_first.Name = "tbtn_first";
            this.tbtn_first.Size = new System.Drawing.Size(23, 22);
            this.tbtn_first.Text = "第一页";
            this.tbtn_first.Click += new System.EventHandler(this.tbtn_first_Click);
            // 
            // tbtn_up
            // 
            this.tbtn_up.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtn_up.Image = global::PagerToolsLibrary.Properties.Resources.up;
            this.tbtn_up.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtn_up.Name = "tbtn_up";
            this.tbtn_up.Size = new System.Drawing.Size(23, 22);
            this.tbtn_up.Text = "上一页";
            this.tbtn_up.Click += new System.EventHandler(this.tbtn_up_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ttxt_current
            // 
            this.ttxt_current.Name = "ttxt_current";
            this.ttxt_current.Size = new System.Drawing.Size(30, 25);
            this.ttxt_current.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ttxt_current.ToolTipText = "当前页";
            this.ttxt_current.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ttxt_current_KeyPress);
            // 
            // tlab_total
            // 
            this.tlab_total.Name = "tlab_total";
            this.tlab_total.Size = new System.Drawing.Size(0, 22);
            this.tlab_total.Tag = "/{0}";
            this.tlab_total.ToolTipText = "总页数";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbtn_next
            // 
            this.tbtn_next.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtn_next.Image = global::PagerToolsLibrary.Properties.Resources.next;
            this.tbtn_next.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtn_next.Name = "tbtn_next";
            this.tbtn_next.Size = new System.Drawing.Size(23, 22);
            this.tbtn_next.Text = "下一页";
            this.tbtn_next.Click += new System.EventHandler(this.tbtn_next_Click);
            // 
            // tbtn_last
            // 
            this.tbtn_last.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtn_last.Image = global::PagerToolsLibrary.Properties.Resources.last;
            this.tbtn_last.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtn_last.Name = "tbtn_last";
            this.tbtn_last.Size = new System.Drawing.Size(23, 22);
            this.tbtn_last.Text = "最后一页";
            this.tbtn_last.Click += new System.EventHandler(this.tbtn_last_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(20, 22);
            this.toolStripLabel2.Text = "第";
            // 
            // tcomb_current
            // 
            this.tcomb_current.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tcomb_current.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tcomb_current.Name = "tcomb_current";
            this.tcomb_current.Size = new System.Drawing.Size(75, 25);
            this.tcomb_current.ToolTipText = "当前页";
            this.tcomb_current.SelectedIndexChanged += new System.EventHandler(this.tcomb_current_SelectedIndexChanged);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(20, 22);
            this.toolStripLabel3.Text = "页";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(32, 22);
            this.toolStripLabel1.Text = "每页";
            // 
            // tcom_num
            // 
            this.tcom_num.Name = "tcom_num";
            this.tcom_num.Size = new System.Drawing.Size(75, 25);
            this.tcom_num.ToolTipText = "每页行数";
            this.tcom_num.SelectedIndexChanged += new System.EventHandler(this.tcom_num_SelectedIndexChanged);
            this.tcom_num.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tcom_num_KeyPress);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(20, 22);
            this.toolStripLabel4.Text = "行";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator5.Visible = false;
            // 
            // tbtn_refresh
            // 
            this.tbtn_refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbtn_refresh.Image = global::PagerToolsLibrary.Properties.Resources.刷新;
            this.tbtn_refresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbtn_refresh.Name = "tbtn_refresh";
            this.tbtn_refresh.Size = new System.Drawing.Size(23, 22);
            this.tbtn_refresh.Text = "toolStripButton5";
            this.tbtn_refresh.Visible = false;
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tlab_datacount
            // 
            this.tlab_datacount.Name = "tlab_datacount";
            this.tlab_datacount.Size = new System.Drawing.Size(0, 22);
            // 
            // PagerToolsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStrip_main);
            this.Name = "PagerToolsControl";
            this.Size = new System.Drawing.Size(712, 25);
            this.toolStrip_main.ResumeLayout(false);
            this.toolStrip_main.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_main;
        private System.Windows.Forms.ToolStripButton tbtn_first;
        private System.Windows.Forms.ToolStripButton tbtn_up;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox ttxt_current;
        private System.Windows.Forms.ToolStripLabel tlab_total;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbtn_next;
        private System.Windows.Forms.ToolStripButton tbtn_last;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox tcomb_current;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox tcom_num;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tbtn_refresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripLabel tlab_datacount;
    }
}
