﻿说明文件
PagerToolsLibrary，用于WinForm DataGridView的分页组件。
Demo，测试项目，使用了DOS.ORM。

PagerToolsLibrary
不涉及数据操作，只是一个数据页数相关操作的组件。
把数据总量、每页行数、当前页数等参数给予组件，当用户改变相关参数时，通过事件监听告诉你相关参数被改变。而数据操作则需另行处理。