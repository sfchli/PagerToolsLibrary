﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dos.ORM;
using SalesSys.Models;

namespace Demo
{
    public partial class Form1 : Form
    {
        private DbSession aDbSession = new DbSession(DatabaseType.Sqlite3
            , @"Data Source=E:\MyCodeForVS\PagerToolsLibrary\Demo\db\test.db;Version=3;");

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random ird = new Random();
            
            for (int i = 0; i < 500; i++)
            {
                TestTable iObj = new TestTable();
                iObj.Col2 = (ird.Next() + i).ToString();
                iObj.Col3 = (ird.Next() + i).ToString();
                iObj.Col4 = (ird.Next() + i).ToString();
                iObj.Col5 = (ird.Next() + i).ToString();
                iObj.Col6 = (ird.Next() + i).ToString();
                iObj.Col7 = (ird.Next() + i).ToString();
                iObj.Col8 = (ird.Next() + i).ToString();
                iObj.Col9 = (ird.Next() + i).ToString();

                aDbSession.Insert<TestTable>(iObj);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int iCount = aDbSession.From<TestTable>().Count();
            pagerToolsControl1.IsTrigger = false;
            pagerToolsControl1.DataCount = iCount;
            pagerToolsControl1.Current = 1;
            pagerToolsControl1.IsTrigger = true;
            LoadData(pagerToolsControl1.PagerNum, 1);
        }

        private void LoadData(int pagnum,int pagindex)
        {
            dataTable1.Clear();
            
            List<TestTable> iListObj = aDbSession.From<TestTable>()
                .Page(pagnum, pagindex)
                .ToList();
            
            foreach(TestTable iObj in iListObj)
            {
                DataRow iRowObj = dataTable1.NewRow();
                iRowObj["Col1"] = iObj.Col1;
                iRowObj["Col2"] = iObj.Col2;
                iRowObj["Col3"] = iObj.Col3;
                iRowObj["Col4"] = iObj.Col4;
                iRowObj["Col5"] = iObj.Col5;
                iRowObj["Col6"] = iObj.Col6;
                iRowObj["Col7"] = iObj.Col7;
                iRowObj["Col8"] = iObj.Col8;
                iRowObj["Col9"] = iObj.Col9;

                dataTable1.Rows.Add(iRowObj);
            }
        }

        private void pagerToolsControl1_PagerIndexChanged(object sender, PagerToolsLibrary.C_EventArgsClass e)
        {
            MessageBox.Show("改变了当前页。" + Environment.NewLine
                + "当前页为：" + e.CurrentPagerIndex.ToString(), "提示"
                , MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            LoadData(pagerToolsControl1.PagerNum, pagerToolsControl1.Current);
        }

        private void pagerToolsControl1_PagerNumChanged(object sender, PagerToolsLibrary.C_EventArgsClass e)
        {
            MessageBox.Show("改变了每页行数。" + Environment.NewLine
                + "当每页行数为：" + e.PagerNum.ToString(), "提示"
                , MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            int iCount = aDbSession.From<TestTable>().Count();
            pagerToolsControl1.IsTrigger = false;
            pagerToolsControl1.DataCount = iCount;
            if (pagerToolsControl1.Current > pagerToolsControl1.Total)
                pagerToolsControl1.Current = pagerToolsControl1.Total;
            pagerToolsControl1.IsTrigger = true;
            LoadData(pagerToolsControl1.PagerNum, pagerToolsControl1.Current);
        }
    }
}
