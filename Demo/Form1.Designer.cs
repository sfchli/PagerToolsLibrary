﻿namespace Demo
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pagerToolsControl1 = new PagerToolsLibrary.PagerToolsControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.col1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col5DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.pagerToolsControl1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1011, 649);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col1DataGridViewTextBoxColumn,
            this.col2DataGridViewTextBoxColumn,
            this.col3DataGridViewTextBoxColumn,
            this.col4DataGridViewTextBoxColumn,
            this.col5DataGridViewTextBoxColumn,
            this.col6DataGridViewTextBoxColumn,
            this.col7DataGridViewTextBoxColumn,
            this.col8DataGridViewTextBoxColumn,
            this.col9DataGridViewTextBoxColumn});
            this.dataGridView1.DataMember = "Table1";
            this.dataGridView1.DataSource = this.dataSet1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 76);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1005, 545);
            this.dataGridView1.TabIndex = 8;
            // 
            // pagerToolsControl1
            // 
            this.pagerToolsControl1.Current = 0;
            this.pagerToolsControl1.DataCount = 0;
            this.pagerToolsControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pagerToolsControl1.IsTrigger = false;
            this.pagerToolsControl1.Location = new System.Drawing.Point(3, 621);
            this.pagerToolsControl1.Name = "pagerToolsControl1";
            this.pagerToolsControl1.PagerNum = 10;
            this.pagerToolsControl1.Size = new System.Drawing.Size(1005, 25);
            this.pagerToolsControl1.TabIndex = 7;
            this.pagerToolsControl1.PagerIndexChanged += new PagerToolsLibrary.C_EventClass.OnPagerIndexChangedEventHandler(this.pagerToolsControl1_PagerIndexChanged);
            this.pagerToolsControl1.PagerNumChanged += new PagerToolsLibrary.C_EventClass.OnPagerNumChangedEventHandler(this.pagerToolsControl1_PagerNumChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1005, 59);
            this.panel1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(148, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "获取数据";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "插入数据";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dataTable1.TableName = "Table1";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Col1";
            this.dataColumn1.ColumnName = "Col1";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Col2";
            this.dataColumn2.ColumnName = "Col2";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Col3";
            this.dataColumn3.ColumnName = "Col3";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Col4";
            this.dataColumn4.ColumnName = "Col4";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Col5";
            this.dataColumn5.ColumnName = "Col5";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "Col6";
            this.dataColumn6.ColumnName = "Col6";
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Col7";
            this.dataColumn7.ColumnName = "Col7";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Col8";
            this.dataColumn8.ColumnName = "Col8";
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Col9";
            this.dataColumn9.ColumnName = "Col9";
            // 
            // col1DataGridViewTextBoxColumn
            // 
            this.col1DataGridViewTextBoxColumn.DataPropertyName = "Col1";
            this.col1DataGridViewTextBoxColumn.HeaderText = "Col1";
            this.col1DataGridViewTextBoxColumn.Name = "col1DataGridViewTextBoxColumn";
            this.col1DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col2DataGridViewTextBoxColumn
            // 
            this.col2DataGridViewTextBoxColumn.DataPropertyName = "Col2";
            this.col2DataGridViewTextBoxColumn.HeaderText = "Col2";
            this.col2DataGridViewTextBoxColumn.Name = "col2DataGridViewTextBoxColumn";
            this.col2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col3DataGridViewTextBoxColumn
            // 
            this.col3DataGridViewTextBoxColumn.DataPropertyName = "Col3";
            this.col3DataGridViewTextBoxColumn.HeaderText = "Col3";
            this.col3DataGridViewTextBoxColumn.Name = "col3DataGridViewTextBoxColumn";
            this.col3DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col4DataGridViewTextBoxColumn
            // 
            this.col4DataGridViewTextBoxColumn.DataPropertyName = "Col4";
            this.col4DataGridViewTextBoxColumn.HeaderText = "Col4";
            this.col4DataGridViewTextBoxColumn.Name = "col4DataGridViewTextBoxColumn";
            this.col4DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col5DataGridViewTextBoxColumn
            // 
            this.col5DataGridViewTextBoxColumn.DataPropertyName = "Col5";
            this.col5DataGridViewTextBoxColumn.HeaderText = "Col5";
            this.col5DataGridViewTextBoxColumn.Name = "col5DataGridViewTextBoxColumn";
            this.col5DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col6DataGridViewTextBoxColumn
            // 
            this.col6DataGridViewTextBoxColumn.DataPropertyName = "Col6";
            this.col6DataGridViewTextBoxColumn.HeaderText = "Col6";
            this.col6DataGridViewTextBoxColumn.Name = "col6DataGridViewTextBoxColumn";
            this.col6DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col7DataGridViewTextBoxColumn
            // 
            this.col7DataGridViewTextBoxColumn.DataPropertyName = "Col7";
            this.col7DataGridViewTextBoxColumn.HeaderText = "Col7";
            this.col7DataGridViewTextBoxColumn.Name = "col7DataGridViewTextBoxColumn";
            this.col7DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col8DataGridViewTextBoxColumn
            // 
            this.col8DataGridViewTextBoxColumn.DataPropertyName = "Col8";
            this.col8DataGridViewTextBoxColumn.HeaderText = "Col8";
            this.col8DataGridViewTextBoxColumn.Name = "col8DataGridViewTextBoxColumn";
            this.col8DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // col9DataGridViewTextBoxColumn
            // 
            this.col9DataGridViewTextBoxColumn.DataPropertyName = "Col9";
            this.col9DataGridViewTextBoxColumn.HeaderText = "Col9";
            this.col9DataGridViewTextBoxColumn.Name = "col9DataGridViewTextBoxColumn";
            this.col9DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 649);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private PagerToolsLibrary.PagerToolsControl pagerToolsControl1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col4DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col5DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col8DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn col9DataGridViewTextBoxColumn;
    }
}

